import numpy as np
import cv, cv2

def rectify(img, points, other_img, other_points):
    """Stereo rectification of images using OpenCV.
    
    Params:
    img:          First image
    points:       Coordinates of selected features in first image
    other_image:  Second image
    other_points: Corresponding features in second image
    
    Returns:      Rectified image pair annotated with points and epipolar lines
    """

    assert points.shape[0] == other_points.shape[0]
    assert points.shape[1] == other_points.shape[1] == 2

    # add an axis
    points2 = points[None,...]
    other_points2 = other_points[None,...]

    # compute fundumental matrix
    F, _ = cv2.findFundamentalMat(other_points2, points2)

    # get homographies 
    _, H1, H2 = cv2.stereoRectifyUncalibrated(other_points2, points2, F, img.shape[:2])

    # expand shape a bit so that warpPerspective does not crop too much
    shape = np.asarray(img.shape[:2])
    shape[0] *= 1.8
    shape = tuple(shape)

    # warp the images using the rectification homographies
    img = cv2.warpPerspective(img, H2, shape)
    other_img = cv2.warpPerspective(other_img, H1, shape)

    # transform points using the rectification homographies
    new_points = np.squeeze(np.dot(H2, add_one_col(points)[:,:,None]).T)
    new_points = new_points / new_points[:,2,None]

    new_other_points = np.squeeze(np.dot(H1, add_one_col(other_points)[:,:,None]).T)
    new_other_points = new_other_points / new_other_points[:,2,None]

    # plot points and lines
    line_color = (255,0,0)
    point_color = (0,0,255)

    for x,y in new_points[:,:2]:
        x = int(np.round(x))
        y = int(np.round(y))
        cv2.line(img, (0,y), (img.shape[1],y), line_color, 1)
        cv2.circle(img, (x,y), 5, point_color, -1)

    for x,y in new_other_points[:,:2]:
        x = int(np.round(x))
        y = int(np.round(y))
        cv2.line(other_img, (0,y), (other_img.shape[1],y), line_color, 1)
        cv2.circle(other_img, (x,y), 5, point_color, -1)

    # arrange images side-by-side
    img_pair = np.hstack((other_img, img))

    return img_pair

def read_floats(f):
    """Read floating point data from file.
    """
    return np.asarray([[float(n) for n in l.strip().split()] for l in f])

def add_one_col(ary):
    """Add a column of ones to array (convert to homogeneous).
    """
    return np.hstack((ary, np.ones((ary.shape[0],1))))

def cv2_imshow(img, window_name="image"):
    """Use OpenCV image viewer.
    """
    window_name += ' (ESC to quit)'

    cv2.namedWindow(window_name, cv.CV_WINDOW_NORMAL)
    cv2.startWindowThread()
    cv2.imshow(window_name,img)
    while True:
        k = cv2.waitKey(100) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()

def main():
    fimga = 'input/pic_a.jpg'
    fimgb = 'input/pic_b.jpg'

    fna = 'input/pts2d-pic_a.txt'
    fnb = 'input/pts2d-pic_b.txt'

    with open(fna, 'r') as f:
        pa = read_floats(f)
    with open(fnb, 'r') as f:
        pb = read_floats(f)

    imga = cv2.imread(fimga, cv2.IMREAD_UNCHANGED)
    imgb = cv2.imread(fimgb, cv2.IMREAD_UNCHANGED)

    img_pair = rectify(imga, pa, imgb, pb)

    cv2_imshow(img_pair, 'Stereo Rectification')

if __name__ == '__main__':
    main()
